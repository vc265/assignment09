<?php

//VARIABLES
$given_array = []; //outside array


//FUNCTIONS
function load_file() {
    //first function loads CSV file into a 2D array
    $multi_array = [];
    $csv_file = fopen("data/archery_terms.csv", "r");
    while(!feof($csv_file)) { //feof tests for end of file
        array_push($multi_array, fgetcsv($csv_file));
    }
    fclose($csv_file);
    return $multi_array;
}

function sort_array($given_array) {
    //second function creates a sorted, associative array from the previous 2D array
    $i; //counter variable
    for ($i = 0; $i <= count($given_array) - 1; $i++) {
        $term = $given_array[$i][0];
        $definition = $given_array[$i][1];
        $assoc_array[$term] = $definition;
    }
    ksort($assoc_array);
    return $assoc_array;
}

function output_list($given_array) {
    //third function creates the HTML output for the dl list
    echo "<dl>";
    foreach ($given_array as $value) {
        echo "<dt>" . array_search($value, $given_array) . "</dt>";
        echo "<dd>" . $value . "</dd>";
        echo "\n";
    }
    echo "</dl>";
}


//FUNCTION CALLS
$first_result = load_file();
$second_result = sort_array($first_result);
$third_result = output_list($second_result);
?>